﻿var usersApp = angular.module('usersApp');

usersApp.directive('serverAlerts', function () {
    return {
        restrict: 'AE',
        replace: 'true',
        templateUrl: '../Content/angular/modules/usersApp/views/serverAlerts.html',
        scope: {
            alerts: '='
        }
    }
});
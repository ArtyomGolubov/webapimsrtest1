﻿var usersApp = angular.module('usersApp');

usersApp.directive('userInfo', function () {
    return {
        restrict: 'AE',
        replace: 'true',
        templateUrl: '../Content/angular/modules/usersApp/views/userInfo.html',
        //scope: {
        //    user: '='
        //}
    }
});
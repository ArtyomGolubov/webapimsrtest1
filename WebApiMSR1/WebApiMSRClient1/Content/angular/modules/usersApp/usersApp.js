﻿var usersApp = angular.module('usersApp', ['ngCookies', 'ngRoute']);

// Example configuration stored as constant
usersApp.constant('config', {
    apiUrl: 'http://localhost:55177/',
    //apiUrl: 'http://www.webapimsr.somee.com/',
    baseUrl: '/',
    //enableDebug: true,
    token: sessionStorage.getItem("tokenInfo"),
    userName: sessionStorage.getItem("nameInfo")
})
.config(['$routeProvider', function ($routeProvider) {
    $routeProvider

    .when('/', {
        templateUrl: '../Content/angular/modules/usersApp/pages/login.html',
        controller: 'accountManagerCtrl'
    })

    .when('/registration', {
        templateUrl: '../Content/angular/modules/usersApp/pages/registration.html',
        controller: 'accountManagerCtrl'
    })

    .when('/changepassword', {
        templateUrl: '../Content/angular/modules/usersApp/pages/changePassword.html',
        controller: 'accountManagerCtrl'
    })

    .when('/usersmanager', {
        templateUrl: '../Content/angular/modules/usersApp/pages/usersmanager.html',
        controller: 'usersManagerCtrl'
    });
}]);

//We already have a limitTo filter built-in to angular,
//let's make a startFrom filter
// для пагинации списка пользователей
usersApp.filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});

//$('.js-tilt').tilt({
//    scale: 2.2
//})


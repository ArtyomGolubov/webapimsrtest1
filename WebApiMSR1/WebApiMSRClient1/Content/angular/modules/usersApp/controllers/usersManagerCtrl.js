﻿var usersApp = angular.module('usersApp');

usersApp.controller('usersManagerCtrl', ['$scope', '$cookies', '$http', '$filter', 'localValueService', function ($scope, $cookies, $http, $filter, localValueService) {

    $scope.usersList = [];
    $scope.rolesList = [];
    $scope.userRolesManagerList = [];

    $scope.getSimpleValues = function () {
        $scope.loader = true;
        localValueService.alerts.length = 0;

        //console.log('usersManagerCtrl.getSimpleValues()');
        var requestData = {
            method: 'GET',
            url: localValueService.config.apiUrl + 'api/values/',
            headers: {
                //'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Bearer ' + localValueService.config.token
            }
        };

        $http(requestData)
        .then(function (data, status, headers, config) {
            $scope.loader = false;
            console.log('success', data);
            localValueService.alerts.push({ type: 'success', msg: 'Данные ' + data.data[0] + ' получены. Статус: ' + data.status });
            $.notify('Данные ' + data.data[0] + ' получены. Статус: ' + data.status, 'success');
        }, function (data, status, headers, config) {
            $scope.loader = false;
            console.log('error', data);
            localValueService.alerts.push({ type: 'danger', msg: 'Данные не получены. Статус: ' + data.status });
            $.notify('Данные не получены. Статус: ' + data.status, 'error');
        });
    }

    // получение пользователей
    $scope.getUsersList = function () {
        $scope.loader = true;
        localValueService.usersList.length = 0;
        localValueService.alerts.length = 0;

        //console.log('usersManagerCtrl.getUsersList()');
        var requestData = {
            method: 'GET',
            url: localValueService.config.apiUrl + 'api/users/',
            headers: {
                //'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Bearer ' + localValueService.config.token
            }
        };

        $http(requestData)
        .then(function (data, status, headers, config) {
            $scope.loader = false;
            //console.log('success', data);
            // вот таким макаром пушим в $scope.usersList
            var l = data.data.length;
            for (var i = 0; i < l; i++) {
                localValueService.usersList.push(JSON.parse(data.data[i]));
            }
            $scope.GetAllRoles();
            $scope.usersList = localValueService.usersList;

            console.log('usersList', $scope.usersList);
            localValueService.alerts.push({ type: 'success', msg: 'Список пользователей получен. Статус: ' + data.status });
            $.notify('Список пользователей получен. Статус: ' + data.status, 'success');
        }, function (data, status, headers, config) {
            $scope.loader = false;
            console.log('error', data);
            localValueService.alerts.push({ type: 'danger', msg: 'При попытке получить список пользователей, возникла ошибка. Статус: ' + data.status });
            $.notify('При попытке получить список пользователей, возникла ошибка. Статус: ' + data.status, 'error');
        });
    }

    // получение ролей
    $scope.GetAllRoles = function() {
        localValueService.rolesList.length = 0;

        var requestData = {
            method: 'GET',
            url: localValueService.config.apiUrl + 'api/roles/',
            headers: {
                //'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Bearer ' + localValueService.config.token
            }
        };

        $http(requestData)
        .then(function (data, status, headers, config) {
            $scope.loader = false;
            //console.log('success', data);
            // вот таким макаром пушим в $scope.rolesList
            var l = data.data.length;
            for (var i = 0; i < l; i++) {
                localValueService.rolesList.push(JSON.parse(data.data[i]));
            }
            $scope.rolesList = localValueService.rolesList;

            console.log('rolesList', $scope.rolesList);
            localValueService.alerts.push({ type: 'success', msg: 'Список ролей получен. Статус: ' + data.status });
            $.notify('Список ролей получен. Статус: ' + data.status, 'success');
        }, function (data, status, headers, config) {
            $scope.loader = false;
            console.log('error', data);
            localValueService.alerts.push({ type: 'danger', msg: 'Список ролей не получен. Статус: ' + data.status });
            $.notify('Список ролей не получен. Статус: ' + data.status, 'error');
        });
    }

    // добавление роли пользователю
    $scope.AddRoleInUser = function (user, role) {
        $scope.loader = true;

        var requestData = {
            method: 'POST',
            url: localValueService.config.apiUrl + 'api/roles/AddRoleInUser',
            headers: {
                //'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Bearer ' + localValueService.config.token
            },
            data: {
                UserId: user.Id,
                RoleId: role.Id
            }
        };

        $http(requestData)
        .then(function (data, status, headers, config) {
            $scope.loader = false;

            // пробую обновить данные на вью
            user.Roles.push(role); // вот где, мляха надо обновить!)
            $scope.ShowRolesManagerView(user); // и пересовать модальное окно

            console.log('data', data);
            localValueService.alerts.push({ type: 'success', msg: 'Роль ' + role.Name + ' для пользователя ' + user.UserName + ' добавлена. Статус: ' + data.status });
            $.notify('Роль ' + role.Name + ' для пользователя ' + user.UserName + ' добавлена. Статус: ' + data.status, 'success');
        }, function (data, status, headers, config) {
            $scope.loader = false;
            console.log('error', data);
            localValueService.alerts.push({ type: 'danger', msg: 'Ошибка при добавлении роли. Статус: ' + data.status });
            $.notify('Ошибка при добавлении роли. Статус: ' + data.status, 'error');
        });
    }

    // удаление роли пользователя
    $scope.RemoveRoleInUser = function (user, role) {
        $scope.loader = true;

        var requestData = {
            method: 'POST',
            url: localValueService.config.apiUrl + 'api/roles/RemoveRoleInUser',
            headers: {
                //'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Bearer ' + localValueService.config.token
            },
            data: {
                UserId: user.Id,
                RoleId: role.Id
            }
        };

        $http(requestData)
        .then(function (data, status, headers, config) {
            $scope.loader = false;

            // пробую обновить данные на вью
            // этот цикл, это - Боль всего джаваскрипта!
            for (var i = 0; i < user.Roles.length; i++) {
                if (user.Roles[i].RoleId == role.Id) {
                    user.Roles.splice(i, 1); // удаляю злосчастную роль у юзера в модальном окне
                }
            }

            $scope.ShowRolesManagerView(user); // и пересовать модальное окно

            console.log('data', data);
            localValueService.alerts.push({ type: 'success', msg: 'Роль ' + role.Name + ' у пользователя ' + user.UserName + ' удалена. Статус: ' + data.status });
            $.notify('Роль ' + role.Name + ' у пользователя ' + user.UserName + ' удалена. Статус: ' + data.status, 'success');
        }, function (data, status, headers, config) {
            $scope.loader = false;
            console.log('error', data);
            localValueService.alerts.push({ type: 'danger', msg: 'Ошибка при удалении роли. Статус: ' + data.status });
            $.notify('Ошибка при удалении роли. Статус: ' + data.status, 'error');
        });
    }

    // содержит ли юзер роль
    $scope.IsContains = function (arrRoles, str) {
        for (var i = 0; i < arrRoles.length; i++) {
            if (arrRoles[i].Name.indexOf(str) >= 0) return true;
        }
        return false;
    }

    // для модального окна управления ролей выбранного пользователя
    $scope.ShowRolesManagerView = function (user) {
        console.log(user);
        $scope.selectedUser = user;
        $scope.userRolesManagerList.length = 0;
        for (var i = 0; i < $scope.rolesList.length; i++) {
            if ($scope.IsContains(user.Roles, $scope.rolesList[i].Name)) {
                $scope.userRolesManagerList.push({ Id: $scope.rolesList[i].RoleId, Name: $scope.rolesList[i].Name, IsContains: true });
            }
            else {
                $scope.userRolesManagerList.push({ Id: $scope.rolesList[i].RoleId, Name: $scope.rolesList[i].Name, IsContains: false });
            }
        }
        console.log('userRolesManagerList', $scope.userRolesManagerList);
    }

    //--- Для пагинации и поиска по фильтру ---//
    $scope.currentPage = 0;
    $scope.pageSize = 10;
    $scope.q = '';

    $scope.getData = function () {
        // needed for the pagination calc
        // https://docs.angularjs.org/api/ng/filter/filter
        return $filter('filter')($scope.usersList, $scope.q)
    }

    $scope.numberOfPages = function () {
        return Math.ceil($scope.getData().length / $scope.pageSize);
    }
    //--//
}]);

///////////////////////////////////////////////////////

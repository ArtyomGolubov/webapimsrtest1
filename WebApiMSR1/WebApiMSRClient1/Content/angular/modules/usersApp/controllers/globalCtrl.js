﻿var usersApp = angular.module('usersApp');

usersApp.controller('globalCtrl', ['$scope', '$cookies', '$http', '$window', '$templateCache', 'localValueService', function ($scope, $cookies, $http, $window, $templateCache, localValueService) {

    console.log(localValueService.config.userName);

    localValueService.config.token ? $scope.isLogin = true : $scope.isLogin = false;
    $scope.name = localValueService.config.userName;

    //{ type: 'danger', msg: 'Oh snap! Change a few things up and try submitting again.' },
    //{ type: 'success', msg: 'Well done! You successfully read this important alert message.' }
    $scope.alerts = localValueService.alerts; // хрен знает как, но автоматически обновляет на вью.

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.Logout = function () {
        localValueService.alerts.length = 0;
        localValueService.usersList.length = 0;
        localValueService.config.token = '';
        localValueService.config.userName = '';
        sessionStorage.removeItem("tokenInfo");
        sessionStorage.removeItem("nameInfo");
        $scope.isLogin = false;
        $scope.loginView = true;

        $window.location.href = '#!/';
    }
}]);
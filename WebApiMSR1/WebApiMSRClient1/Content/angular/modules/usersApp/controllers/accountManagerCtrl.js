﻿var usersApp = angular.module('usersApp');

usersApp.controller('accountManagerCtrl', ['$scope', '$cookies', '$http', '$window', '$rootScope', '$templateCache', 'localValueService', function ($scope, $cookies, $http, $window, $rootScope, $templateCache, localValueService) {

    console.log(localValueService.config.userName);

    localValueService.config.token ? $scope.isLogin = true : $scope.isLogin = false;

    if ($scope.isLogin == true && $window.location.hash == '#!/') {
        // для редиректа
        $window.location.href = '#!/usersmanager';
    }

    
    $scope.name = localValueService.config.userName;
    $scope.email = '';
    $scope.password = '';
    $scope.confirmPassword = '';

    //регистрация
    $scope.Registration = function () {

        $scope.loader = true;
        localValueService.alerts.length = 0;
        
        var registerData = {
            Email: $scope.email,
            Password: $scope.password,
            ConfirmPassword: $scope.confirmPassword
        };

        $http.post(localValueService.config.apiUrl + 'api/Account/Register', registerData, {})
        .then(function (data, status, headers, config) {
            $scope.loader = false;
            console.log(data);
            localValueService.alerts.push({ type: 'success', msg: 'Регистрация для ' + data.config.data.Email + ' пройдена. Статус: ' + data.status });
            $.notify('Регистрация для ' + data.config.data.Email + ' пройдена. Статус: ' + data.status, 'success');

            $window.location.href = '#!/';
        }, function (data, status, headers, config) {
            $scope.loader = false;
            console.log('Data', data);
            localValueService.alerts.push({ type: 'danger', msg: 'В процесе регистрации возникла ошибка. Статус: ' + data.status });
            $.notify('В процесе регистрации возникла ошибка. Статус: ' + data.status, 'error');
        });
    }

    //авторизация
    $scope.Login = function () {

        $scope.loader = true;
        localValueService.alerts.length = 0;

        // вот такой вот пистец нужен. Иначе в ответе "unsupported_grant_type"
        var loginData = {
            method: 'POST',
            url: localValueService.config.apiUrl + 'Token',
            data: 'grant_type=password&username=' + encodeURIComponent($scope.email) + '&password=' + encodeURIComponent($scope.password),
        };

        $http(loginData)
        .then(function (data, status, headers, config) {
            $scope.loader = false;
            console.log(data);
            $scope.name = data.data.userName;

            localValueService.alerts.push({ type: 'success', msg: 'Авторизация для ' + $scope.name + ' пройдена. Статус: ' + data.status });
            $.notify('Авторизация для ' + $scope.name + ' пройдена. Статус: ' + data.status, 'success');
            localValueService.alerts.push({ type: 'info', msg: 'Token: ' + data.data.access_token });
            $.notify('Token: ' + data.data.access_token, 'info');

            // сохраняем в хранилище sessionStorage токен доступа
            sessionStorage.setItem("tokenInfo", data.data.access_token);
            sessionStorage.setItem("nameInfo", $scope.name);
            // костыль для обновления данных в localValueService
            localValueService.config.userName = $scope.name;
            localValueService.config.token = data.data.access_token;

            console.log(data.data.access_token);

            $scope.isLogin = true;

            $scope.$parent.name = $scope.name; // передаем значения в globalCtrl
            $scope.$parent.isLogin = true;

            // на всякий пожарный обнулим
            $scope.password = '';
            $scope.confirmPassword = '';

            $window.location.href = '#!/usersmanager';
        }, function (data, status, headers, config) {
            $scope.loader = false;
            console.log('Data', data);
            localValueService.alerts.push({ type: 'danger', msg: 'В процесе авторизации возникла ошибка. Статус: ' + data.status });
            $.notify('В процесе авторизации возникла ошибка. Статус: ' + data.status, 'error');
        });
    }

    $scope.ChangePassword = function () {
        console.log('ChangePassword()');

        $scope.loader = true;
        localValueService.alerts.length = 0;

        var requestData = {
            method: 'POST',
            url: localValueService.config.apiUrl + 'api/Account/ChangePassword',
            headers: {
                //'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Bearer ' + localValueService.config.token
            },
            data: {
                OldPassword: $scope.oldPassword,
                NewPassword: $scope.password,
                ConfirmPassword: $scope.confirmPassword
            }
        };

        $http(requestData)
        .then(function (data, status, headers, config) {
            $scope.loader = false;
            console.log(data);
            localValueService.alerts.push({ type: 'success', msg: 'Пароль для ' + localValueService.config.userName + ' изменен. Статус: ' + data.status });
            $.notify('Пароль для ' + localValueService.config.userName + ' изменен. Статус: ' + data.status, 'success');

            $window.location.href = '#!/';
        }, function (data, status, headers, config) {
            $scope.loader = false;
            console.log('Data', data);
            localValueService.alerts.push({ type: 'danger', msg: 'В процесе изменения пароля возникла ошибка. Статус: ' + data.status });
            $.notify('В процесе изменения пароля возникла ошибка. Статус: ' + data.status, 'error');
        });
    }

    $scope.Logout = function () {
        localValueService.alerts.length = 0;
        localValueService.usersList.length = 0;
        localValueService.config.token = '';
        localValueService.config.userName = '';
        sessionStorage.removeItem("tokenInfo");
        sessionStorage.removeItem("nameInfo");
        $scope.isLogin = false;
        $scope.loginView = true;

        $window.location.href = '#!/';
    }
}]);
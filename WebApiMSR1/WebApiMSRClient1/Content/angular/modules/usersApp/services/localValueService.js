﻿var usersApp = angular.module('usersApp');

usersApp.service('localValueService', function (config) {
    var self = {
        config: config,
        alerts: [],
        usersList: [],
        rolesList: []
    };

    return self;
});
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiMSR1.Services;

namespace WebApiMSR1.Controllers
{
    //[Authorize(Roles = "admin")]
    [RoutePrefix("api/roles")]
    public class RolesController : ApiController
    {
        // как и другие контроллеры, содержит в себе вот такой сервис
        private readonly AccountsService accountsService;

        public RolesController()
        {
            accountsService = new AccountsService();
        }

        // GET api/roles
        public IEnumerable<string> Get()
        {
            var roles = accountsService.GetAllRoles();

            List<string> json = new List<string>();

            foreach (var role in roles)
            {
                var obj = JsonConvert.SerializeObject(role);
                json.Add(obj);
            }

            return json;
        }

        // GET api/roles/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/roles
        public void Post([FromBody]string value)
        {
        }

        // PUT api/roles/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/roles/5
        public void Delete(int id)
        {
        }

        [Route("AddRoleInUser")]
        [HttpPost]
        public void AddRoleInUser([FromBody]UserAndRoleRequest request)
        {
            accountsService.AddRoleInUser(request.UserId, request.RoleId);
        }

        [Route("RemoveRoleInUser")]
        [HttpPost]
        public void RemoveRoleInUser([FromBody]UserAndRoleRequest request)
        {
            accountsService.RemoveRoleInUser(request.UserId, request.RoleId);
        }
    }

    public class UserAndRoleRequest
    {
        public string UserId { get; set; }
        public string RoleId { get; set; }
    }
}

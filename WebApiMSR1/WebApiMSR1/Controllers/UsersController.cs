﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiMSR1.Services;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json;
using WebApiMSR1.Models;
using WebApiMSR1.Models.DTO;
using AutoMapper;

namespace WebApiMSR1.Controllers
{
    [Authorize(Roles = "admin")]
    public class UsersController : ApiController
    {
        // как и другие контроллеры, содержит в себе вот такой сервис
        private readonly AccountsService accountsService;

        public UsersController()
        {
            accountsService = new AccountsService();
        }

        // GET api/users
        public IEnumerable<string> Get()
        {
            var users = accountsService.GetUsers();

            List<string> json = new List<string>();

            foreach (var user in users)
            {
                var obj = JsonConvert.SerializeObject(user);
                json.Add(obj);
            }
            
            return json;
        }

        // GET api/users/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/users
        public void Post([FromBody]string value)
        {
        }

        // PUT api/users/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/users/5
        public void Delete(int id)
        {
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiMSR1.Interfaces
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        T GetById(int id);
        T GetById(string id);
        IEnumerable<T> Find(Func<T, bool> predicate);
        void Create(T item);
        void Update(T item);
        void Delete(long id);

        //IEnumerable<Models.ApplicationUser> GetAll();
    }
}
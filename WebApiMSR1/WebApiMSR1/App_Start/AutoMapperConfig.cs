﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using WebApiMSR1.Models;
using WebApiMSR1.Models.DTO;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel;

namespace WebApiMSR1
{
    public class AutoMapperConfig
    {
        // регистрируем мапперы
        public static void RegisterMappings()
        {
            //Mapper.CreateMap<ApplicationUser, User>(); // из ApplicationUser в User
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<IdentityUserRole, IdentityRole>();
                cfg.CreateMap<ApplicationUser, UserDTO>();
                cfg.CreateMap<IdentityUserRole, RolesDTO>();
                cfg.CreateMap<IdentityRole, RolesDTO>()
                .ForMember("RoleId", opt => opt.MapFrom(u => u.Id));
                //.ForMember(dest => dest.Roles, opt => opt.MapFrom(src => src.Roles)); // невыполнимое условие src.Roles.Count < 0
                //cfg.AddProfile<MainProfile>();
            });

            //var config = new MapperConfiguration(cfg => {
            //    cfg.CreateMap<ApplicationUser, User>();
            //    cfg.AddProfile<MainProfile>();
            //});
        }
    }

    public static class IgnoreReadOnlyExtensions
    {
        public static IMappingExpression<TSource, TDestination> IgnoreReadOnly<TSource, TDestination>(
                   this IMappingExpression<TSource, TDestination> expression)
        {
            var sourceType = typeof(TSource);

            foreach (var property in sourceType.GetProperties())
            {
                PropertyDescriptor descriptor = TypeDescriptor.GetProperties(sourceType)[property.Name];
                ReadOnlyAttribute attribute = (ReadOnlyAttribute)descriptor.Attributes[typeof(ReadOnlyAttribute)];
                if (attribute.IsReadOnly == true)
                    expression.ForMember(property.Name, opt => opt.Ignore());
            }
            return expression;
        }
    }
}
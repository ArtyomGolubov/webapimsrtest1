﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebApiMSR1.Interfaces;
using WebApiMSR1.Models;
using WebApiMSR1.Models.DTO;
using WebApiMSR1.Models.Repositories;

namespace WebApiMSR1.Services
{
    public class AccountsService
    {
        public IUnitOfWork UnitOfWork { get; set; }

        public AccountsService()
        {
            UnitOfWork = new EFUnitOfWork();
        }

        #region User
        public UserDTO GetUser(string id)
        {
            if (id == null)
                throw new ValidationException("Не установлено id пользователя");

            var user = UnitOfWork.Users.GetById(id);

            if (user == null)
                throw new ValidationException("Пользователь не найден");
            var userDTO = Mapper.Map<ApplicationUser, UserDTO>(user);
            userDTO.Roles = GetAllRolesForUser(user.Id); // по красоте бы это делать в AutoMapper
            return userDTO;
        }

        public UserDTO GetUser(Func<ApplicationUser, bool> predicate)
        {
            ApplicationUser user = UnitOfWork.Users.Find(predicate).FirstOrDefault();
            var userDTO = Mapper.Map<ApplicationUser, UserDTO>(user);
            userDTO.Roles = GetAllRolesForUser(user.Id); // по красоте бы это делать в AutoMapper
            return userDTO;
        }

        public IEnumerable<UserDTO> GetUsers()
        {
            var users = UnitOfWork.Users.GetAll();
            var usersDTO = Mapper.Map<IEnumerable<ApplicationUser>, IEnumerable<UserDTO>>(users);
            foreach(var user in usersDTO)
            {
                user.Roles = GetAllRolesForUser(user.Id); // по красоте бы это делать в AutoMapper
            }
            return usersDTO;
        }

        public void EditUser(UserDTO user)
        {
            UnitOfWork.Users.Update(Mapper.Map<UserDTO, ApplicationUser>(user));
            UnitOfWork.Save();
        }

        public IEnumerable<RolesDTO> GetAllRoles()
        {
            var roles = UnitOfWork.Roles.GetAll();
            return Mapper.Map< IEnumerable<IdentityRole>, IEnumerable<RolesDTO>>(roles);
        }

        public IEnumerable<RolesDTO> GetAllRolesForUser(string UserId)
        {
            var user = UnitOfWork.Users.GetById(UserId);
            var roles = Mapper.Map<IEnumerable<IdentityUserRole>, IEnumerable<RolesDTO>>(user.Roles);
            foreach (var role in roles)
            {
                roles.FirstOrDefault(r => r.RoleId == role.RoleId).Name = UnitOfWork.Roles.GetById(role.RoleId).Name;
            }
            return roles;
        }

        public void AddRoleInUser(string UserId, string RoleId)
        {
            var user = UnitOfWork.Users.GetById(UserId);
            var role = new IdentityUserRole();
            role.UserId = UserId;
            role.RoleId = RoleId;
            user.Roles.Add(role);
            UnitOfWork.Save();
        }

        public void RemoveRoleInUser(string UserId, string RoleId)
        {
            var user = UnitOfWork.Users.GetById(UserId);
            var role = user.Roles.FirstOrDefault(r => r.RoleId == RoleId); // в методе user.Roles.Remove(role); находит роль для удаления только полученную таким образом. var role = new IdentityUserRole(); - НЕ ПОДХОДИТ.
            var res = user.Roles.Remove(role);
            UnitOfWork.Save();
        }


            #endregion
        }
    }
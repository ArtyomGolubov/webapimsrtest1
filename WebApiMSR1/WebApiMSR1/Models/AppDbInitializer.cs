﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApiMSR1.Models
{
    // ВНИМАНИЕ!
    //после сноса базы нужно расскоментить class AppDbInitializer : DropCreateDatabaseAlways<ApplicationDbContext>
    // чтобы создалась новая база. После создания нужно снова ЗАКОМЕНТИТЬ эту строку и РАСКОМЕНТИТЬ
    // class AppDbInitializer : DropCreateDatabaseIfModelChanges<ApplicationDbContext>

    // Для Миграции 
    //public class AppDbInitializer : MigrateDatabaseToLatestVersion<ApplicationDbContext, Configuration>

    // Если понадобилось заюзать эти классы, то комментим в миграции ткла методов Up() и Down()
    //class AppDbInitializer : DropCreateDatabaseAlways<ApplicationDbContext>
    public class AppDbInitializer : DropCreateDatabaseIfModelChanges<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            // создаем две роли
            var role1 = new IdentityRole { Name = "admin" };
            var role2 = new IdentityRole { Name = "user" };
            //var role3 = new IdentityRole { Name = "moder" };

            // добавляем роли в бд
            roleManager.Create(role1);
            roleManager.Create(role2);
            //roleManager.Create(role3);

            // создаем пользователей
            var admin = new ApplicationUser { Email = "admin@gmail.com", UserName = "ADMIN" };
            string password = "111111";
            var result = userManager.Create(admin, password);

            // если создание пользователя прошло успешно
            if (result.Succeeded)
            {
                // добавляем для пользователя роль
                userManager.AddToRole(admin.Id, role1.Name);
                userManager.AddToRole(admin.Id, role2.Name);
                //userManager.AddToRole(admin.Id, role3.Name);
            }

            base.Seed(context);
        }
    }
}
﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiMSR1.Interfaces;

namespace WebApiMSR1.Models.Repositories
{
    // класс, который содержит в себе все репозитории и передает им всем один контекст
    public class EFUnitOfWork : IUnitOfWork
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserRepository _applicationUserRepository;
        private UserRoleRepository _userRoleRepository;

        public IRepository<ApplicationUser> Users
        {
            get { return _applicationUserRepository ?? (_applicationUserRepository = new ApplicationUserRepository(db)); }
        }

        public IRepository<IdentityRole> Roles
        {
            get { return _userRoleRepository ?? (_userRoleRepository = new UserRoleRepository(db)); }
        }

        public void Save()
        {
            db.SaveChanges();
        }

        private bool _disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                this._disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiMSR1.Interfaces;

namespace WebApiMSR1.Models.Repositories
{
    public class UserRoleRepository : IRepository<IdentityRole>
    {
        private ApplicationDbContext context;
        private UserStore<ApplicationUser> store;
        private RoleManager<IdentityRole> manager;

        public UserRoleRepository(ApplicationDbContext context)
        {
            this.context = context;
            store = new UserStore<ApplicationUser>(context);
            manager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
        }

        public IEnumerable<IdentityRole> GetAll()
        {
            return manager.Roles;
        }

        public IdentityRole GetById(int id) { return null; }

        public IdentityRole GetById(string id)
        {
            return manager.Roles.FirstOrDefault(u => u.Id == id);
        }

        public IEnumerable<IdentityRole> Find(Func<IdentityRole, Boolean> predicate)
        {
            return manager.Roles.Where(predicate).ToList();
        }

        public void Create(IdentityRole role) {
            // добавляем роль в бд
            manager.Create(role);
        }

        public void Update(IdentityRole role)
        {
            manager.Update(role);
        }

        public void Delete(long id) { }

        public void Delete(string id)
        {
            IdentityRole role = manager.Roles.FirstOrDefault(u => u.Id != null && u.Id == id);
            manager.Delete(role);
        }
    }
}
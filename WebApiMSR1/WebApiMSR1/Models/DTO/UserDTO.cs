﻿using AutoMapper;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiMSR1.Models.DTO
{
    public class UserDTO
    {
        public string Email { get; set; }
        public string Id { get; set; }
        public string UserName { get; set; }

        [IgnoreMap]
        public IEnumerable<RolesDTO> Roles { get; set; }

        public UserDTO()
        {
            Roles = new List<RolesDTO>();
        }
    }
}
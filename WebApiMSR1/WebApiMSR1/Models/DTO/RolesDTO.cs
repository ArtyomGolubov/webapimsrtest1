﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiMSR1.Models.DTO
{
    public class RolesDTO
    {
        public string UserId { get; set; }
        public string RoleId { get; set; }
        public string Name { get; set; }
    }
}